//
//  AGTMainViewController.h
//  Teclado
//
//  Created by Fernando Rodríguez Romero on 26/02/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AGTMainViewController : UIViewController<UITextFieldDelegate>

-(IBAction)removeKeyBoard:(id)sender;
@end
