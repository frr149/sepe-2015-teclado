//
//  AGTMainViewController.m
//  Teclado
//
//  Created by Fernando Rodríguez Romero on 26/02/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import "AGTMainViewController.h"

@interface AGTMainViewController ()
@property (weak, nonatomic) IBOutlet UITextField *bugView;

@end

@implementation AGTMainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    // Configuramos el textfield
    self.bugView.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bug.png"]];
    self.bugView.placeholder = @"Describa Ud su bug, maifrén";
    self.bugView.delegate = self;
    
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    // El usuario le ha dado a into
    // ¿LO damos por bueno?
    
    // Valimos el texto y si está bien, entonces...
    [textField resignFirstResponder];
    return YES;
    
}


-(void) textFieldDidEndEditing:(UITextField *)textField{
    
    // El tio ha terminado de editar y YA HEMOS VALIDADO
    // Momento de guardar el texto en algun lugar
    NSLog(@"Ha escrito: %@", textField.text);
}

-(IBAction)removeKeyBoard:(id)sender{
    
    // aquí hago desaparecer el teclado
    [self.view endEditing:YES];
    
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
